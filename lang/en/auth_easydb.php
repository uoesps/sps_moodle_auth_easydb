<?php

/**
 * Strings for component 'auth_easydb', language 'en'.
 *
 * @package  auth_easydb
 * @author   Keith Tait
 */

$string['auth_easydbcantconnect'] = 'Could not connect to the specified authentication database...';
$string['auth_easydbdebugauthdb'] = 'Debug ADOdb';
$string['auth_easydbdebugauthdbhelp'] = 'Debug ADOdb connection to external database - use when getting empty page during login. Not suitable for production sites.';
$string['auth_easydbdeleteuser'] = 'Deleted user {$a->name} id {$a->id}';
$string['auth_easydbdeleteusererror'] = 'Error deleting user {$a}';
$string['auth_easydbdescription'] = 'This method uses the EASE server to check whether a given username and password is valid. The accounts are initially populated from an externa; databse.';
$string['auth_easydbextencoding'] = 'External db encoding';
$string['auth_easydbextencodinghelp'] = 'Encoding used in external database';
$string['auth_easydbextrafields'] = 'These fields are optional.  You can choose to pre-fill some Moodle user fields with information from the <b>external database fields</b> that you specify here. <p>If you leave these blank, then defaults will be used.</p><p>In either case, the user will be able to edit all of these fields after they log in.</p>';
$string['auth_easydbfieldpass'] = 'Name of the field containing passwords';
$string['auth_easydbfieldpass_key'] = 'Password field';
$string['auth_easydbfielduser'] = 'Name of the field containing usernames';
$string['auth_easydbfielduser_key'] = 'Username field';
$string['auth_easydbhost'] = 'The computer hosting the database server. Use a system DSN entry if using ODBC.';
$string['auth_easydbhost_key'] = 'Host';
$string['auth_easydbchangepasswordurl_key'] = 'Password-change URL';
$string['auth_easydbinsertuser'] = 'Inserted user {$a->name} id {$a->id}';
$string['auth_easydbinsertuserduplicate'] = 'Error inserting user {$a->username} - user with this username was already created through \'{$a->auth}\' plugin.';
$string['auth_easydbinsertusererror'] = 'Error inserting user {$a}';
$string['auth_easydbname'] = 'Name of the database itself. Leave empty if using an ODBC DSN.';
$string['auth_easydbname_key'] = 'DB name';
$string['auth_easydbpass'] = 'Password matching the above username';
$string['auth_easydbpass_key'] = 'Password';
$string['auth_easydbpasstype'] = '<p>Specify the format that the password field is using. MD5 hashing is useful for connecting to other common web applications like PostNuke.</p> <p>Use \'internal\' if you want to the external DB to manage usernames &amp; email addresses, but Moodle to manage passwords. If you use \'internal\', you <i>must</i> provide a populated email address field in the external DB, and you must execute both admin/cron.php and auth/db/cli/sync_users.php regularly. Moodle will send an email to new users with a temporary password.</p>';
$string['auth_easydbpasstype_key'] = 'Password format';
$string['auth_easydbreviveduser'] = 'Revived user {$a->name} id {$a->id}';
$string['auth_easydbrevivedusererror'] = 'Error reviving user {$a}';
$string['auth_easydbsetupsql'] = 'SQL setup command';
$string['auth_easydbsetupsqlhelp'] = 'SQL command for special database setup, often used to setup communication encoding - example for MySQL and PostgreSQL: <em>SET NAMES \'utf8\'</em>';
$string['auth_easydbsuspenduser'] = 'Suspended user {$a->name} id {$a->id}';
$string['auth_easydbsuspendusererror'] = 'Error suspending user {$a}';
$string['auth_easydbsybasequoting'] = 'Use sybase quotes';
$string['auth_easydbsybasequotinghelp'] = 'Sybase style single quote escaping - needed for Oracle, MS SQL and some other databases. Do not use for MySQL!';
$string['auth_easydbtable'] = 'Name of the table in the database';
$string['auth_easydbtable_key'] = 'Table';
$string['auth_easydbtype'] = 'The database type (See the <a href="http://phplens.com/adodb/supported.databases.html" target="_blank">ADOdb documentation</a> for details)';
$string['auth_easydbtype_key'] = 'Database';
$string['auth_easydbupdatinguser'] = 'Updating user {$a->name} id {$a->id}';
$string['auth_easydbuser'] = 'Username with read access to the database';
$string['auth_easydbuser_key'] = 'DB user';
$string['auth_easydbusernotexist'] = 'Cannot update non-existent user: {$a}';
$string['auth_easydbuserstoadd'] = 'User entries to add: {$a}';
$string['auth_easydbuserstoremove'] = 'User entries to remove: {$a}';
$string['pluginname'] = 'EasyDB';
