<?php

/**
 * Authentication Plugin: Cosign/EASE Authentication
 *
 * Syncs data from an external DB
 *
 * @package  auth_easydb
 * @author   Keith Tait
 * @author   Martin Dougiamas
 * @license  http://www.gnu.org/copyleft/gpl.html GNU Public License
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/authlib.php');

/**
 * EasyDB authentication plugin.
 */
class auth_plugin_easydb extends auth_plugin_base {

  /**
   * Constructor.
   */
  function __construct() {
    global $CFG;
    require_once($CFG->libdir.'/adodb/adodb.inc.php');

    $this->authtype = 'easydb';
    $this->config = get_config('auth/easydb');

    if (empty($this->config->extencoding)) {
      $this->config->extencoding = 'utf-8';
    }
  }

  /**
   * Returns true if this authentication plugin can change the user's
   * password.
   *
   * @return bool
   */
  function can_change_password() {
    return false;
  }

  /**
   * Returns true if plugin allows resetting of internal password.
   *
   * @return bool
   */
  function can_reset_password() {
    return false;
  }

  /**
   * Returns the URL for changing the user's pw, or empty if the default can
   * be used.
   *
   * @return moodle_url
   */
  function change_password_url() {
    return null;
  }

  /**
   * Prints a form for configuring this authentication plugin.
   *
   * This function is called from admin/auth.php, and outputs a full page with
   * a form for configuring this plugin.
   *
   * @param stdClass $config
   * @param array $err errors
   * @param array $user_fields
   * @return void
   */
  function config_form($config, $err, $user_fields) {
    include 'config.html';
  }

  /**
   * Connect to external database.
   *
   * @return ADOConnection
   */
  function db_init() {
    // Connect to the external database (forcing new connection).
    $authdb = ADONewConnection($this->config->type);

    if (!empty($this->config->debugauthdb)) {
      $authdb->debug = true;
      ob_start(); //Start output buffer to allow later use of the page headers.
    }

    $authdb->Connect($this->config->host, $this->config->user, $this->config->pass, $this->config->name, true);
    $authdb->SetFetchMode(ADODB_FETCH_ASSOC);

    if (!empty($this->config->setupsql)) {
      $authdb->Execute($this->config->setupsql);
    }

    return $authdb;
  }

  /**
   * Returns user attribute mappings between moodle and ldap.
   *
   * @return array
   */
  function db_attributes() {
    $moodleattributes = array();

    foreach ($this->userfields as $field) {
      if (!empty($this->config->{"field_map_$field"})) {
          $moodleattributes[$field] = $this->config->{"field_map_$field"};
      }
    }

    $moodleattributes['username'] = $this->config->fielduser;

    return $moodleattributes;
  }

  /**
   * Add slashes, we can not use placeholders or system functions.
   *
   * @param string $text
   * @return string
   */
  function ext_addslashes($text) {
    if (empty($this->config->sybasequoting)) {
      $text = str_replace('\\', '\\\\', $text);
      $text = str_replace(array('\'', '"', "\0"), array('\\\'', '\\"', '\\0'), $text);
    } else {
      $text = str_replace("'", "''", $text);
    }

    return $text;
  }

  /**
   * Reads any other information for a user from external database,
   * then returns it in an array.
   *
   * @param string $username
   * @return array
   */
  function get_userinfo($username) {
    global $CFG;

    $extusername = core_text::convert($username, 'utf-8', $this->config->extencoding);

    $authdb = $this->db_init();

    // Array to map local fieldnames we want, to external fieldnames.
    $selectfields = $this->db_attributes();

    $result = array();

    // If at least one field is mapped from external db, get that mapped data.
    if ($selectfields) {
      $select = array();

      foreach ($selectfields as $localname=>$externalname) {
        $select[] = "$externalname AS $localname";
      }

      $select = implode(', ', $select);
      $sql = "SELECT $select FROM {$this->config->table} WHERE {$this->config->fielduser} = '".$this->ext_addslashes($extusername)."'";

      if ($rs = $authdb->Execute($sql)) {
        if (!$rs->EOF) {
          $fields_obj = $rs->FetchObj();
          $fields_obj = (object)array_change_key_case((array)$fields_obj , CASE_LOWER);

          foreach ($selectfields as $localname=>$externalname) {
            $result[$localname] = core_text::convert($fields_obj->{$localname}, $this->config->extencoding, 'utf-8');
          }
        }

        $rs->Close();
      }
    }

    $authdb->Close();

    return $result;
  }

  /**
   * Reads user information from DB and return it in an object.
   *
   * @param string $username username
   * @return array
   */
  function get_userinfo_asobj($username) {
    $user_array = truncate_userinfo($this->get_userinfo($username));
    $user = new stdClass();

    foreach($user_array as $key=>$value) {
      $user->{$key} = $value;
    }

    return $user;
  }

  function get_userlist() {
    // Init result value.
    $result = array();
    $authdb = $this->db_init();
    $rs = $authdb->Execute("SELECT {$this->config->fielduser} AS username FROM {$this->config->table} ");

    if (!$rs) {
      print_error('auth_easydbcantconnect','auth_easydb');
    } else if (!$rs->EOF) {
      while ($rec = $rs->FetchRow()) {
        $rec = (object)array_change_key_case((array)$rec , CASE_LOWER);
        array_push($result, $rec->username);
      }
    }

    $authdb->Close();

    return $result;
  }

  /**
   * Returns true if this authentication plugin is "internal".
   *
   * Internal plugins use password hashes from Moodle user table for authentication.
   *
   * @return bool
   */
  function is_internal() {
    return false;
  }

  /**
   * Indicates if moodle should automatically update internal user
   * records with data from external sources using the information
   * from auth_plugin_base::get_userinfo().
   *
   * @return bool true means automatically copy data from ext to user table
   */
  function is_synchronised_with_external() {
      return true;
  }

  /**
   * Hook for overriding behaviour of login page.
   * This method is called from login/index.php page for all enabled auth plugins.
   *
   * @global object
   * @return bool
   */
  function loginpage_hook() {
    global $CFG, $frm, $OUTPUT, $PAGE, $SESSION;

    if(isset($_SERVER['REMOTE_USER'])) {
      $frm->username = $_SERVER['REMOTE_USER'] ;
      $frm->password = 'passwdEASE';

      return;
    }
  }

  /**
   * Pre logout hook.
   * This method is called from require_logout() for all enabled auth plugins,
   *
   * @global string
   */
  function prelogout_hook() {
    global $redirect;

    setcookie($_SERVER['COSIGN_SERVICE'], "null", time()-1, '/', "", 1 );

    $redirect = "https://www.ease.ed.ac.uk/logout/logout.cgi";
  }

  /**
   * Indicates if password hashes should be stored in local moodle database.
   * @return bool
   */
  function prevent_local_passwords() {
    return true;
  }

  /**
   * Processes and stores configuration data for this authentication plugin.
   *
   * @param srdClass $config
   * @return bool always true or exception
   */
  function process_config($config) {
    // Set to defaults if undefined
    if (!isset($config->host)) {
        $config->host = 'localhost';
    }

    if (!isset($config->type)) {
        $config->type = 'mysql';
    }

    if (!isset($config->sybasequoting)) {
        $config->sybasequoting = 0;
    }

    if (!isset($config->name)) {
        $config->name = '';
    }

    if (!isset($config->user)) {
        $config->user = '';
    }

    if (!isset($config->pass)) {
        $config->pass = '';
    }

    if (!isset($config->table)) {
        $config->table = '';
    }

    if (!isset($config->fielduser)) {
        $config->fielduser = '';
    }

    if (!isset($config->fieldpass)) {
        $config->fieldpass = '';
    }

    if (!isset($config->passtype)) {
        $config->passtype = 'plaintext';
    }

    if (!isset($config->extencoding)) {
        $config->extencoding = 'utf-8';
    }

    if (!isset($config->setupsql)) {
        $config->setupsql = '';
    }

    if (!isset($config->debugauthdb)) {
        $config->debugauthdb = 0;
    }

    if (!isset($config->removeuser)) {
        $config->removeuser = AUTH_REMOVEUSER_KEEP;
    }

    if (!isset($config->changepasswordurl)) {
        $config->changepasswordurl = '';
    }

    // Save settings.
    set_config('host',          $config->host,          'auth/easydb');
    set_config('type',          $config->type,          'auth/easydb');
    set_config('sybasequoting', $config->sybasequoting, 'auth/easydb');
    set_config('name',          $config->name,          'auth/easydb');
    set_config('user',          $config->user,          'auth/easydb');
    set_config('pass',          $config->pass,          'auth/easydb');
    set_config('table',         $config->table,         'auth/easydb');
    set_config('fielduser',     $config->fielduser,     'auth/easydb');
    set_config('fieldpass',     $config->fieldpass,     'auth/easydb');
    set_config('passtype',      $config->passtype,      'auth/easydb');
    set_config('extencoding',   trim($config->extencoding), 'auth/easydb');
    set_config('setupsql',      trim($config->setupsql),'auth/easydb');
    set_config('debugauthdb',   $config->debugauthdb,   'auth/easydb');
    set_config('removeuser',    $config->removeuser,    'auth/easydb');
    set_config('changepasswordurl', trim($config->changepasswordurl), 'auth/easydb');

    return true;
  }

  /**
   * Synchronizes user from external db to moodle user table.
   *
   * Sync should be done by using idnumber attribute, not username.
   * You need to pass firstsync parameter to function to fill in
   * idnumbers if they don't exists in moodle user table.
   *
   * Syncing users removes (disables) users that don't exists anymore in external db.
   * Creates new users and updates coursecreator status of users.
   *
   * This implementation is simpler but less scalable than the one found in the LDAP module.
   *
   * @param progress_trace $trace
   * @param bool $do_updates  Optional: set to true to force an update of existing accounts
   * @return int 0 means success, 1 means failure
   */
  function sync_users(progress_trace $trace, $do_updates=false) {
    global $CFG, $DB;

    // List external users.
    $userlist = $this->get_userlist();

    // Delete obsolete internal users.
    if (!empty($this->config->removeuser)) {
      $suspendselect = "";

      if ($this->config->removeuser == AUTH_REMOVEUSER_SUSPEND) {
        $suspendselect = "AND u.suspended = 0";
      }

      // Find obsolete users.
      if (count($userlist)) {
        list($notin_sql, $params) = $DB->get_in_or_equal($userlist, SQL_PARAMS_NAMED, 'u', false);
        $params['authtype'] = $this->authtype;
        $sql = "SELECT u.* FROM {user} u WHERE u.auth=:authtype AND u.deleted=0 AND u.mnethostid=:mnethostid $suspendselect AND u.username $notin_sql";
      } else {
        $sql = "SELECT u.* FROM {user} u WHERE u.auth=:authtype AND u.deleted=0 AND u.mnethostid=:mnethostid $suspendselect";
        $params = array();
        $params['authtype'] = $this->authtype;
      }

      $params['mnethostid'] = $CFG->mnet_localhost_id;
      $remove_users = $DB->get_records_sql($sql, $params);

      if (!empty($remove_users)) {
        require_once($CFG->dirroot.'/user/lib.php');
        $trace->output(get_string('auth_easydbuserstoremove','auth_easydb', count($remove_users)));

        foreach ($remove_users as $user) {
          if ($this->config->removeuser == AUTH_REMOVEUSER_FULLDELETE) {
            delete_user($user);
            $trace->output(get_string('auth_easydbdeleteuser', 'auth_easydb', array('name'=>$user->username, 'id'=>$user->id)), 1);
          } else if ($this->config->removeuser == AUTH_REMOVEUSER_SUSPEND) {
            $updateuser = new stdClass();
            $updateuser->id   = $user->id;
            $updateuser->suspended = 1;

            user_update_user($updateuser, false);

            $trace->output(get_string('auth_easydbsuspenduser', 'auth_easydb', array('name'=>$user->username, 'id'=>$user->id)), 1);
          }
        }
      }

      unset($remove_users);
    }

    if (!count($userlist)) {
      // Exit right here, nothing else to do.
      $trace->finished();
      return 0;
    }

    // Update existing accounts.
    if ($do_updates) {
      // Narrow down what fields we need to update.
      $all_keys = array_keys(get_object_vars($this->config));
      $updatekeys = array();

      foreach ($all_keys as $key) {
        if (preg_match('/^field_updatelocal_(.+)$/',$key, $match)) {
          if ($this->config->{$key} === 'onlogin') {
            array_push($updatekeys, $match[1]); // The actual key name.
          }
        }
      }

      unset($all_keys); unset($key);

      // Only go ahead if we actually have fields to update locally.
      if (!empty($updatekeys)) {
        list($in_sql, $params) = $DB->get_in_or_equal($userlist, SQL_PARAMS_NAMED, 'u', true);

        $params['authtype'] = $this->authtype;
        $sql = "SELECT u.id, u.username FROM {user} u WHERE u.auth=:authtype AND u.deleted=0 AND u.username {$in_sql}";

        if ($update_users = $DB->get_records_sql($sql, $params)) {
          $trace->output("User entries to update: ".count($update_users));

          foreach ($update_users as $user) {
            if ($this->update_user_record($user->username, $updatekeys)) {
              $trace->output(get_string('auth_easydbupdatinguser', 'auth_easydb', array('name'=>$user->username, 'id'=>$user->id)), 1);
            } else {
              $trace->output(get_string('auth_easydbupdatinguser', 'auth_easydb', array('name'=>$user->username, 'id'=>$user->id))." - ".get_string('skipped'), 1);
            }
          }

          unset($update_users);
        }
      }
    }

    // Create missing accounts.
    // NOTE: this is very memory intensive and generally inefficient.
    $suspendselect = "";

    if ($this->config->removeuser == AUTH_REMOVEUSER_SUSPEND) {
      $suspendselect = "AND u.suspended = 0";
    }

    $sql = "SELECT u.id, u.username FROM {user} u WHERE u.auth=:authtype AND u.deleted='0' AND mnethostid=:mnethostid $suspendselect";
    $users = $DB->get_records_sql($sql, array('authtype'=>$this->authtype, 'mnethostid'=>$CFG->mnet_localhost_id));

    // Simplify down to usernames.
    $usernames = array();

    if (!empty($users)) {
      foreach ($users as $user) {
        array_push($usernames, $user->username);
      }

      unset($users);
    }

    $add_users = array_diff($userlist, $usernames);

    unset($usernames);

    if (!empty($add_users)) {
      $trace->output(get_string('auth_easydbuserstoadd','auth_easydb',count($add_users)));

      // Do not use transactions around this foreach, we want to skip problematic users, not revert everything.
      foreach($add_users as $user) {
        $username = $user;

        if ($this->config->removeuser == AUTH_REMOVEUSER_SUSPEND) {
          if ($old_user = $DB->get_record('user', array('username'=>$username, 'deleted'=>0, 'suspended'=>1, 'mnethostid'=>$CFG->mnet_localhost_id, 'auth'=>$this->authtype))) {
            $DB->set_field('user', 'suspended', 0, array('id'=>$old_user->id));
            $trace->output(get_string('auth_easydbreviveduser', 'auth_easydb', array('name'=>$username, 'id'=>$old_user->id)), 1);
            continue;
          }
        }

        // Do not try to undelete users here, instead select suspending if you ever expect users will reappear.

        // Prep a few params.
        $user = $this->get_userinfo_asobj($user);
        $user->username   = $username;
        $user->confirmed  = 1;
        $user->auth       = $this->authtype;
        $user->mnethostid = $CFG->mnet_localhost_id;

        if (empty($user->lang)) {
          $user->lang = $CFG->lang;
        }

        if (empty($user->calendartype)) {
          $user->calendartype = $CFG->calendartype;
        }

        $user->timecreated = time();
        $user->timemodified = $user->timecreated;

        if ($collision = $DB->get_record_select('user', "username = :username AND mnethostid = :mnethostid AND auth <> :auth", array('username'=>$user->username, 'mnethostid'=>$CFG->mnet_localhost_id, 'auth'=>$this->authtype), 'id,username,auth')) {
          $trace->output(get_string('auth_easydbinsertuserduplicate', 'auth_easydb', array('username'=>$user->username, 'auth'=>$collision->auth)), 1);
          continue;
        } try {
          $id = $DB->insert_record ('user', $user); // it is truly a new user
          $trace->output(get_string('auth_easydbinsertuser', 'auth_easydb', array('name'=>$user->username, 'id'=>$id)), 1);
        } catch (moodle_exception $e) {
          $trace->output(get_string('auth_easydbinsertusererror', 'auth_easydb', $user->username), 1);
          continue;
        }

        // If relevant, tag for password generation.
        if ($this->is_internal()) {
          set_user_preference('auth_forcepasswordchange', 1, $id);
          set_user_preference('create_password',          1, $id);
        }

        // Make sure user context is present.
        context_user::instance($id);
      }

      unset($add_users);
    }

    $trace->finished();

    return 0;
  }

  /**
   * Test if settings are ok, print info to output.
   * @private
   */
  public function test_settings() {
    global $CFG, $OUTPUT;

    // NOTE: this is not localised intentionally, admins are supposed to understand English at least a bit...

    raise_memory_limit(MEMORY_HUGE);

    if (empty($this->config->table)) {
      echo $OUTPUT->notification('External table not specified.', 'notifyproblem');
      return;
    }

    if (empty($this->config->fielduser)) {
      echo $OUTPUT->notification('External user field not specified.', 'notifyproblem');
      return;
    }

    $olddebug = $CFG->debug;
    $olddisplay = ini_get('display_errors');

    ini_set('display_errors', '1');

    $CFG->debug = DEBUG_DEVELOPER;
    $olddebugauthdb = $this->config->debugauthdb;
    $this->config->debugauthdb = 1;

    error_reporting($CFG->debug);

    $adodb = $this->db_init();

    if (!$adodb or !$adodb->IsConnected()) {
      $this->config->debugauthdb = $olddebugauthdb;
      $CFG->debug = $olddebug;

      ini_set('display_errors', $olddisplay);
      error_reporting($CFG->debug);
      ob_end_flush();

      echo $OUTPUT->notification('Cannot connect the database.', 'notifyproblem');

      return;
    }

    $rs = $adodb->Execute("SELECT * FROM {$this->config->table} WHERE {$this->config->fielduser} <> 'random_unlikely_username'"); // Any unlikely name is ok here.

    if (!$rs) {
      echo $OUTPUT->notification('Can not read external table.', 'notifyproblem');
    } else if ($rs->EOF) {
      echo $OUTPUT->notification('External table is empty.', 'notifyproblem');
      $rs->close();
    } else {
      $fields_obj = $rs->FetchObj();
      $columns = array_keys((array)$fields_obj);
      echo $OUTPUT->notification('External table contains following columns:<br />'.implode(', ', $columns), 'notifysuccess');
      $rs->close();
    }

    $adodb->Close();

    $this->config->debugauthdb = $olddebugauthdb;
    $CFG->debug = $olddebug;

    ini_set('display_errors', $olddisplay);
    error_reporting($CFG->debug);
    ob_end_flush();
  }

  /**
   * will update a local user record from an external source.
   * is a lighter version of the one in moodlelib -- won't do
   * expensive ops such as enrolment.
   *
   * If you don't pass $updatekeys, there is a performance hit and
   * values removed from DB won't be removed from moodle.
   *
   * @param string $username username
   * @param bool $updatekeys
   * @return stdClass
   */
  function update_user_record($username, $updatekeys=false) {
    global $CFG, $DB;

    //just in case check text case
    $username = trim(core_text::strtolower($username));

    // get the current user record
    $user = $DB->get_record('user', array('username'=>$username, 'mnethostid'=>$CFG->mnet_localhost_id));

    if (empty($user)) { // trouble
      error_log("Cannot update non-existent user: $username");
      print_error('auth_easydbusernotexist','auth_easydb',$username);
      die;
    }

    // Ensure userid is not overwritten.
    $userid = $user->id;
    $updated = false;

    if ($newinfo = $this->get_userinfo($username)) {
      $newinfo = truncate_userinfo($newinfo);

      if (empty($updatekeys)) { // All keys? This does not support removing values.
        $updatekeys = array_keys($newinfo);
      }

      foreach ($updatekeys as $key) {
        if (isset($newinfo[$key])) {
          $value = $newinfo[$key];
        } else {
          $value = '';
        }

        if (!empty($this->config->{'field_updatelocal_' . $key})) {
          if (isset($user->{$key}) and $user->{$key} != $value) { // Only update if it's changed.
            $DB->set_field('user', $key, $value, array('id'=>$userid));
            $updated = true;
          }
        }
      }
    }

    if ($updated) {
      $DB->set_field('user', 'timemodified', time(), array('id'=>$userid));
    }

    return $DB->get_record('user', array('id'=>$userid, 'deleted'=>0));
  }

  function user_authenticated_hook(&$user, $username, $password) {
    // system("/var/www/mgc/courses/auth/ease/cli/runsync");
  }

  function user_exists($username) {
    // Init result value.
    $result = false;
    $extusername = core_text::convert($username, 'utf-8', $this->config->extencoding);
    $authdb = $this->db_init();
    $rs = $authdb->Execute("SELECT * FROM {$this->config->table} WHERE {$this->config->fielduser} = '".$this->ext_addslashes($extusername)."' ");

    if (!$rs) {
      //print_error('auth_easydbcantconnect','auth_easydb');
    } else if (!$rs->EOF) {
      // User exists externally.
      $result = true;
    }

    $authdb->Close();

    return $result;
  }

  /**
   * Returns true if the username and password work and false if they are
   * wrong or don't exist.
   *
   * @param string $username The username
   * @param string $password The password
   * @return bool Authentication success or failure.
   */
  function user_login($username, $password) {
    $result = false;

    if ($this->user_exists($username)) {
      $result = true;
    }

    return $result;
  }

  /**
   * Called when the user record is updated.
   * Modifies user in external database. It takes olduser (before changes) and newuser (after changes)
   * compares information saved modified information to external db.
   *
   * @param stdClass $olduser     Userobject before modifications
   * @param stdClass $newuser     Userobject new modified userobject
   * @return boolean result
   *
   */
  function user_update($olduser, $newuser) {
    return false;
  }

  /**
   * Change a user's password.
   *
   * @param  stdClass  $user      User table object
   * @param  string  $newpassword Plaintext password
   * @return bool                 True on success
   */
  function user_update_password($user, $newpassword) {
    return false;
  }

  /**
   * A chance to validate form data, and last chance to
   * do stuff before it is inserted in config_plugin
   *
   * @param stfdClass $form
   * @param array $err errors
   * @return void
   */
  function validate_form($form, &$err) {
    if ($form->passtype === 'internal') {
      $this->config->changepasswordurl = '';
      set_config('changepasswordurl', '', 'auth/easydb');
    }
  }

}
