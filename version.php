<?php

/**
 * Version details
 *
 * @package  auth_easydb
 * @author   Keith Tait
 */

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2014100100;        // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2013110500;        // Requires this Moodle version
$plugin->component = 'auth_easydb';     // Full name of the plugin (used for diagnostics)
